# TRANSPONDER SWITCH

simple script to select the state of the transponder

* ON / OFF / PULSE
* Transponder field name is human readable
* Works well with the 3 positions switch
* Pulse Transponder 3 times and shut off.

---

## Requirements

### Chips

* YOLOL chip (basic)

### Inputs

* `:TrS`: Transponder State, from a button or Switch `ButtonState` or `SwitchState`. [-1=PULSE, 0=OFF, 1=ON]

### Outputs

* `:Transponder` : The State of the Transponder.

---

## Limits

* Lines take 0.2 seconds to execute.
* A line can contain a maximum of 70 characters (comments and spaces included).

---

## Algorythm / Explanation

Line 1 wait for the `:TrS` to not be 0 to start the script.

* `:TrS`  : State of the button/Switch for the Transponder.

Line 2 turn the `:Transponder` ON and check if it need to pulse.

Line 3-6 pulse the `:Transponder`

Line 7 turn OFF the `:Transponder` and `:TrS`
