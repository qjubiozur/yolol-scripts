# SIMPLE RESSOURCES PERCENTAGE

Utility simple script to convert Ressources to Percentage.

* Display Ressources in %.
* Consider all ressources to have the same field name, and same usage/values.
* Useful when there is one or a limited amount of parts.

---

## Requirements

### Chips

* YOLOL chip (basic)

### Inputs

* `:Prop`: Propellant Tank Support field `GasNetworkStoredResource`.
* `:PropMax`: Propellant Tank Support `GasNetworkMaxResource`.
* `:Bat`: Battery field `StoredBatteryPower`.
* `:BatMax`: Battery field `MaxBatteryPower`.

### Outputs

* `:Batteries` : Percentage Value of the Batteries power ressources.
* `:Propellant`: Percentage value of the Propellant resource.

---

## Limits

Designed to be simple, and consider that all the Ressources storage have the same fields name and the equal usage/recharge.

* Lines take 0.2 seconds to execute.
* A line can contain a maximum of 70 characters (comments and spaces included).

---

## Algorythm / Explanation

Line 1 is precalculation that doesn't need to be done every loops.

* `b`  : Percalculation for the batteries %.
* `p`  : Precalculation for the Propellant %.

Line 2 is the percentage calculation which loop on itself.

* `:Batteries` : Percentage Value of the Batteries power ressources.
* `:Propellant`: Percentage value of the Propellant resource.
