# POWER MANAGER

Utility script to help manage the power system of a ship.

* Somewhat Expendable (Easy editing to expand the amount of batteries/Fuel Generator Chambers)
* Works with individual Fuel Generator Chamber Buttons (Required)
* Works with a General Shutdown Button (Optional)
* Display usage and storage in %
* Manage fuel usage depending on need
* Battery Minimum Threshold before starting the generators

---

## Requirements

### Chips

* YOLOL chip (basic)

### Inputs

`#` Represent a number to manage separate similar parts.

* `:Bat#`: Battery field `StoredBatteryPower`.
* `:BatMax#`: Battery field `MaxBatteryPower`.
* `:Fuel#`: Fuel Generator Chamber field `FuelChamberFuel`.
* `:FuelMax#`: Fuel Generator Chamber field `FuelChamberMaxFuel`.
* `:Gen#`: Fuel Generator Chamber field `FuelChamberUnitRate`.
* `:GenON#`: Button field `ButtonState`. `1`: ON, `0`: OFF.
* `:Shutdown`: Button field `ButtonState`. `1`: Shutdown, `0`: Active.

### Outputs

`#` Represent a number to manage separate similar parts.

* `:GenMax#`: Fuel Generator Chamber field `FuelChamberUnitRateLimit`.
* `:Generators`: Percentage value of the generator usage.
* `:Fuel`: Percentage value of the fuel ressource.
* `:Batteries` : Percentage Value of the Batteries power ressources.

---

## Tweaks

### Add/Remove Fuel Generator Chambers

Make sure each Fuel Generator Chambers have their field names accordingly with the correct number.

* On line 1: `gm=?*p` replace ? with the total amount of Fuel Generator Chambers on the ship.
* On line 3: `fm=:FuelMax#+...` Add/Remove :FuelMax# as needed.
* On line 7: `:GenMax#=gr*:GenON#` Add/Remove as needed.
* On line 8: `gc=:Gen#+...` Add/Remove :Gen# as needed.
* On line 9: `fc=:Fuel#+...` Add/Remove :Fuel# as needed.

### Add/Remove Batteries

Make sure each Battery have their field names accordingly with the correct number.

* On line 2: `bm=:BatMax#+...` Add/Remove :BatMax# as needed.
* On line 5: `bc=:Bat#+...` Add/Remove :Bat# as needed.

### Change Batteries Threshold

You can change the threshold of the batteries stored power, this allow you to tweak when the power generation kicks in.

* On line 2: `bt=0.75` change 0.75 for the percentage (0=0%, 1=100%) of stored power.

### Remove Per Generator Power Button

If you don't want to need to use a power on button for each generators, you can edit the script as follow.

* On line 7: `:GenMax#=gr*:GenON#` remove the `*:GenON#` for each generators.

### Shutdown Button

The Shutdown button is optional as the default will be 0 if the field doesn't exist.

* On line 6: `...*(NOT :Shutdown)` You can remove this part if you prefer to remove the shutdown button.

---

## Limits

Due to the limits in the number of characters per line, it may not be possible to add a lot of differents Batteries/Fuel Chambers. It may require to do some heavy modification to the code.

* Lines take 0.2 seconds to execute.
* A line can contain a maximum of 70 characters (comments and spaces included).

---

## Algorythm / Explanation

Line 1 to 3 are precalculation that doesn't need to be done every loops

* `p`  : Represent the value of 100 for percentage (Less character usage in the code)
* `gm` : Represent the Generator Max Rate Limit (To calculate :Generators)
* `bt` : Represent the battery threshold (check the teack part). % of the max battery power.
* `bm` : Max battery power
* `b`  : The battery threshold in number
* `bd` : Battery threshold pre calculation
* `fm` : Max fuel

Line 5 is managing the batteries values

* `bc` : Current Battery power
* `:Batteries` : Battery Power %, used to display with a Progress Bar screen.

Line 6-7 is processing and setting the value for the max rate limit of each Fuel Generator Chambers. If the Batteries are lower than the threshold, then Generator Chambers kicks in and only power to a Fuel saving rate (More batteries power = Less fuel)

* `gr` : The generators rate
* `:GenMax#` : Set the Rate limit field for each Generator Chambers

Line 8 is managing the Generators values

* `gc` : The current generator rate
* `:Generators` : Generator rate %, used to display with a Progress Bar screen.

Line 9 is managing the Fuel values

* `fc` : The current fuel
* `:Fuel` : Fuel value in %, used to display with a Progress Bar screen.

Line 10 is looping back to line 5 to avoid all the precalculations

---

## Credits

Some of the formula where inspired by the [Starbase Wiki](https://wiki.starbasegame.com/index.php/Common_YOLOL)
