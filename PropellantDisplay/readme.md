# COOLING MANAGER

Utility script to display the total Propellant Ressource in a percentage Value.

* Somewhat Expendable (Easy editing to expand the amount of Propellant Tank Supports)
* Display Ressources in %.

---

## Requirements

### Chips

* YOLOL chip (basic)

### Inputs

`#` Represent a number to manage separate similar parts.

* `:Prop#`: Propellant Tank Support field `GasNetworkStoredResource`.
* `:PropMax#`: Propellant Tank Support `GasNetworkMaxResource`.

### Outputs

`#` Represent a number to manage separate similar parts.

* `:Propellant`: Percentage value of the Propellant resource.

---

## Tweaks

### Add/Remove Propellant Tank Support

Make sure each Propellant Tank Support have their field names accordingly with the correct number. You can rename only one of the support if you use multiple per tanks.

* On line 1: `mp=:PropMax#+...` Add/Remove :PropMax# as needed.
* On line 3: `cp=:Prop#+` Add/Remove :Prop# as needed.

---

## Limits

Due to the limits in the number of characters per line, it may not be possible to add a lot of differents Propellant Tanks Supports. It may require to do some heavy modification to the code.

* Lines take 0.2 seconds to execute.
* A line can contain a maximum of 70 characters (comments and spaces included).

---

## Algorythm / Explanation

Line 1 is precalculation that doesn't need to be done every loops.

* `p`  : Represent the value of 100 for percentage (Less character usage in the code).
* `mp` : Represent the maximum value for the Propellant resource.

Line 3 is managing the Radiatiors Values.

* `cp` : Current Propellant ressource.
* `:Propellant` : Propellant ressources in %, used to display with a Progress Bar screen.

Line 4 is looping back to line 3 to avoid all the precalculations
