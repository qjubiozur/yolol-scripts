# STARBASE YOLOL SCRIPTS

My collection of scripts for the game Starbase.

## Ingame information

* Name: Qchan
* Base: Origin 21 (Not that it matter)

## License

All my scripts are licensed under GPLv3, unless some part of the code have been inspired by another script with a more restrictive/open license.

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)

## Ressources

* [This Repo](https://bitbucket.org/qjubiozur/yolol-scripts/)
* [Starbase Website](https://www.starbasegame.com/)
* [Starbase YOLOL Wiki](https://wiki.starbasegame.com/index.php/YOLOL)
* [Repo Yolol-is-Cyclon](https://github.com/Jerald/yolol-is-cylons)
* [Repo Cyclon YOLOL](https://github.com/CylonSB/Scripts)
