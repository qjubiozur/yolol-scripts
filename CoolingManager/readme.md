# COOLING MANAGER

Utility script to help manage the cooling system of a ship.

* Somewhat Expendable (Easy editing to expand the amount of Cooling Racks/Radiators)
* Managing of Cooling Racks depending on needs.
* Display usage and storage in %

---

## Requirements

### Chips

* YOLOL chip (basic)

### Inputs

`#` Represent a number to manage separate similar parts.

* `:Rad#`: Radiators field `RadiationRate`.
* `:RadMax#`: Radiators field `RadiationRateLimit`.
* `:RadCool#`: Radiators field `StoredRadiatorCoolant`.
* `:RadCoolMax#`: Radiators field `MaxRadiatorCoolant`.
* `:Cooler#`: Cooling Rack field `CoolerUnitRate`.

### Outputs

`#` Represent a number to manage separate similar parts.

* `:CoolerMax#`: Cooling Rack field `CoolerUnitRateLimit`.
* `:Radiators`: Percentage value of the Radiators usage.
* `:CoolingRacks`: Percentage value of the Cooling Racks usage.
* `:RadiatorCoolant`: Percentage value of the Radiators Collant resources.

---

## Tweaks

### Add/Remove Radiators

Make sure each Radiators have their field names accordingly with the correct number. There is no points in renaming the Radiators extensions at the moment.

* On line 1: `mr=?*p` replace ? with the total amount of Radiator Bases on the ship.
* On line 2: `mrc=:RadCoolMax#` Add/Remove :RadCoolMax# as needed.
* On line 4: `rc=:Rad#+...` Add/Remove :Rad# as needed.
* On line 8: `crc=:RadCool#+...` Add/Remove :RadCool# as needed.

### Add/Remove Cooling Racks

Make sure each Cooling Racks have their field names accordingly with the correct number.

* On line 1: `mc=?*p` replace ? with the total amount of Cooling Racks on the ship.
* On line 6: `:CoolerMax#=cr` Add/Remove as needed.
* On line 7: `cc=:Cooler#+...` Add/Remove :Cooler# as needed.

### Change Radiators Threshold

You can change the threshold of the Radiators. if the Radiators get above that threshold, the Cooling Racks will start being active. (0=0%, 1=100%)

* On line 1: `rt=0.90` change 0.90 for the percentage (0=0%, 1=100%) of Radiation.

---

## Limits

Due to the limits in the number of characters per line, it may not be possible to add a lot of differents Radiators/Cooling Racks. It may require to do some heavy modification to the code.

* Lines take 0.2 seconds to execute.
* A line can contain a maximum of 70 characters (comments and spaces included).

---

## Algorythm / Explanation

Line 1 to 2 are precalculation that doesn't need to be done every loops.

* `p`  : Represent the value of 100 for percentage (Less character usage in the code).
* `mr` : Represent the maximum value for the Radiators usage.
* `mc` : Represent the maximum value for the Cooling Racks usage.
* `rt` : Radiator Threshold in percentage.
* `r`  : Radiator Threshold in number.
* `rd` : Radiator Pre Calculation for cooling Rack rate.
* `mrc`: Radiators max Coolant

Line 4 is managing the Radiatiors Values.

* `rc` : Radiators current usage.
* `:Radiators` : Radiators usage in %, used to display with a Progress Bar screen.

Line 5 to 6 are for managing the activation of the Cooling Racks.

* `cr` : Usage rate to use for the Cooling Racks.
* `:CoolerMax#` : Set the Rate Limit for the Cooling Racks

Line 7 is for managing Cooling Racks Values.

* `cc` : Current Cooling Racks usage.
* `:CoolingRacks` : Cooling Racks usage in %, used to display with a Progress Bar screen.

Line 8 is for managing Radiators Coolants Values

* `crc` : Current Radiators coolant stored
* `:RadiatorCoolant` : Radiators coolant usage in %, used to display with a Progress Bar screen.

Line 9 is looping back to line 4 to avoid all the precalculations
