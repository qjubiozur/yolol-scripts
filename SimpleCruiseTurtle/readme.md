# SIMPLE CRUISE TURTLE

Simple script to manage Cruise mode, Turtle Mode and Shutdown

* (De)Activate Cruise Control
* Turtle mode to slowdown all movements
* Shutdown button
* Doesn't require Hybrid buttons

---

## Requirements

### Chips

* YOLOL chip (basic)

### Inputs

* `:Cruise`: Button field `ButtonState` as a 0 or 1.
* `:Shutdown`: Button field `ButtonState` as a 0 or 1.
* `:Turtle`: Button field `ButtonState` as a 0 or 1.
* `:FcuForward`: Flight Control Unit field `FcuForward`.

### Outputs

* `:FcuGeneralMultiplier` : Flight Control Unit field `FcuGeneralMultiplier` for turtle mode.
* `:Thrust`: Percentage value of the Thrust.
* `:LeverCruise`: Lever Field `LeverCenteringSpeed` for the forward Thrust.

---

## Tweaks

### Speed Up/Down the non cruise mode lever

Make sure the fields of the lever for the forward Thrust are named correctly.

* On line 1: `lrs=?` replace ? with the speed you would like (Lower is slower).

### Change the turtle mode speed ration

Make sure the fields of the flight control unit are named correctly.

* On line 1: `ss=?` replace ? with the Value you would like (0 is %, 100 is 100%).

---

## Limits

* Lines take 0.2 seconds to execute.
* A line can contain a maximum of 70 characters (comments and spaces included).

---

## Algorythm / Explanation

Line 1 is variables for tweaks.
Line 2 is precalculation.

* `ss`  : The complement value for the turtle mode speed.

Line 3 is managing the cruise and Propellant.

* `:LeverCruise` : Set the Lever Centering speed to it's value depending on the Cruise mode.
* `:PropOpen`    : Shutdown or open the Propellant tank depending on the shutdown value.

Line 4 is setting up the turtle mode

* `:FcuGeneralMultiplier` : Set the multiplier for the thruster output.

Line 5 is Updating the Thurst Value and looping back to 3

* `:Thrust` : Percentage value of Thrust.
